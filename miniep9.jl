#Mini Ep 9

#   Função vista em aula
function multiplica(a, b)
    dima = size(a)
    dimb = size(b)
    if dima[2] != dimb[1]
        return -1
    end
    c = zeros(dima[1], dimb[2])
    for i in 1:dima[1]
        for j in 1:dimb[2]
            for k in 1:dima[2]
                c[i, j] = c[i, j] + a[i, k] * b[k, j]
            end
        end
    end
    return c
end

#   Função para calcular a potenciação de 2 matrizes
#       metodo basico de potenciação
function matrix_pot(M, p)
    while p > 1
        M = multiplica(M, M)
        p = p - 1
    end
    return M
end

#Função para calcular potenciação utilizando o metodo da exponenciação ao quadrado
function matrix_pot_by_squaring(M, p)
    if p == 1
        return M
    elseif p % 2 == 0
        return matrix_pot_by_squaring(multiplica(M, M), p / 2)
    elseif p % 2 == 1
        return multiplica(M, matrix_pot_by_squaring(multiplica(M, M), (p - 1) / 2))
    end
end


#   TESTES
using Test

    #Função de testes para as funções matrix_pot e matrix_pot_by_squaring
    function test()
        M = Matrix(LinearAlgebra.I, 25, 25)

        @test matrix_pot([1 2 ; 3 4], 1) == [1 2 ; 3 4]
        @test matrix_pot([1 2 ; 3 4], 2) == [7.0 10.0 ; 15.0 22.0]
        @test length(matrix_pot([4 8 0 4 ; 8 4 9 6 ; 9 6 4 0 ; 9 5 4 7], 7)) == 16
        @test matrix_pot(M, 235) == M

        @test matrix_pot_by_squaring([1 2 ; 3 4], 1) == [1 2 ; 3 4]
        @test matrix_pot_by_squaring([1 2 ; 3 4], 2) == [7.0 10.0 ; 15.0 22.0]
        @test length(matrix_pot_by_squaring([4 8 0 4 ; 8 4 9 6 ; 9 6 4 0 ; 9 5 4 7], 7)) == 16
        @test matrix_pot_by_squaring(M, 235) == M
    end

    
using LinearAlgebra

    #Função para comparar tempo entre as duas funções
    function compare_times()
        caminho = pwd()
        open(string(caminho, "/teste.txt"), "w") do f
            for i in 2:45

                M = Matrix(LinearAlgebra.I, i, i)
                a = @elapsed matrix_pot(M, abs(30 - 45) + 1)
                b = @elapsed matrix_pot_by_squaring(M, abs(30 - 45) + 1)

                t1 = "\nBasico => Tempo M^" * string(i) * " foi de: " * string(a) * "\n"
                t2 = "Squaring => Tempo M^" * string(i) * " foi de: " * string(b) * "\n"

                write(f, t1 * t2)
            end
        end
    end
